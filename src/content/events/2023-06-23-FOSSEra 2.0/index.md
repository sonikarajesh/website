---
path: /events/FOSSEra 2.0/
date: "2023-06-23"
datestring: "23-24 June 2023"
cover: "./poster.jpg"
author: "sonikarajesh"
name: "Sonika Rajesh"
title: "FOSSEra 2.0"
---

![Poster](./Banner.png)
The Free and Open Source Software Cell of our college under the sponsorship of ICFOSS, Government of Kerala is planning to conduct a 2 day event for the students of our college. The event will act as an introduction for students to the world of free and open source software.

On the first day, a session will be conducted for students on the topic ‘Introduction to OpenStreetMap’ and it will help them in understanding the scope of Free and Open Source Software. The hands-on sessions will be handled by Manoj Karingamadathil (Free Software Activist, Director of Sahya Digital Conservation Foundation).

In the evening of the first day, a 24 hour hackathon - Hackfiesta v2.0 will be kicked off. The same will continue till 3:30 pm, next day. After the event, we plan to have an engaging talk by one of our alumni from the Computer Science department and past student coordinator of  FOSS Cell, Jayaraj J.



🗓️ **DATE and TIME:** June 23 2023 09:00 AM - June 24 2023 05:00 PM